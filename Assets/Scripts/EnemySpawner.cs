using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private int min = 1;
    [SerializeField] private int max = 5;
    [SerializeField] private Transform[] spawnPoints;
    [SerializeField] private GameObject enemy;
    [SerializeField] private LevelGoalController levelGoalController;

    private int EnemyNumber { get; set; }

    private void Awake()
    {
        EnemyNumber = Random.Range(min, max);
        for (var i = 0; i < EnemyNumber; i++)
        { 
            Instantiate(enemy, spawnPoints[i].position, transform.rotation);
        }
        levelGoalController.SetSpawnedEnemies(EnemyNumber);
    }

}
