using System;
using UnityEngine;

public class Combat : MonoBehaviour
{
    [SerializeField] private Transform meleeHitbox;
    [SerializeField] private LayerMask enemy;
    private CharacterAnimation _characterAnimation;
    private float _timeForNextAttack;

    private void Awake()
    {
        _characterAnimation = GetComponentInChildren<CharacterAnimation>();
    }

    public void MeleeAttack(int attackDamage, float attackRange, float attackDelay)
    {
        if (_timeForNextAttack > Time.time) return;
        _characterAnimation.MeleeAttack();
        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(meleeHitbox.position, attackRange, enemy);
        foreach (var element in hitEnemies)
        {
            element.GetComponent<Enemy>().GetDamage(attackDamage);
        }
        _timeForNextAttack = Time.time + attackDelay;
    }

    public void RangeAttack()
    {
        throw new NotImplementedException();
    }
}
