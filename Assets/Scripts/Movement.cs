using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] private Transform meleeHitbox;
    private readonly Vector3 _rightAttack = new (0.5f, 1.5f, 0);
    private readonly Vector3 _leftAttack = new (-0.3f, 1.5f, 0);
    private SpriteRenderer _spriteRenderer;
    private Rigidbody2D _rigidbody;

    private void Awake()
    { 
        _spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    public void Run(float speed, float horizontalMove)
    {
        transform.position += speed * Time.deltaTime * new Vector3(horizontalMove, 0, 0);
        meleeHitbox.localPosition = _rightAttack;
        if (horizontalMove != 0)
        {
            _spriteRenderer.flipX = horizontalMove < 0;
        }
        if (_spriteRenderer.flipX)
        {
            meleeHitbox.localPosition = _leftAttack;
        }
    }

    public void Jump(float jumpForce)
    {
        _rigidbody.AddForce(transform.up * jumpForce, ForceMode2D.Impulse);
    }
}
