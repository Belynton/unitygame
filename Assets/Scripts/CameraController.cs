using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform player;

    private Vector3 _position;

    private void Awake()
    {
        if (!player) 
        {
            player = FindObjectOfType<Character>().transform;
        }
    }
    private void Update()
    {
        _position = player.position;
        _position.z = -10f;
        transform.position = Vector3.Lerp(transform.position, _position, Time.deltaTime);
    }
}
