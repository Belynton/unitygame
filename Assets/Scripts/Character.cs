using System;
using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField] private float speed = 3f;
    [SerializeField] private float jumpForce = 13f;
    [SerializeField] private float attackRange = 0.5f;
    [SerializeField] private int attackDamage = 2;
    [SerializeField] private float attackDelay = 2f;
    [SerializeField] private float deathHeightLevel = -20f;
    [SerializeField] private Collider2D footCollider;
    [SerializeField] private LayerMask ground;
    private LevelGoalController _levelGoalController;
    private CharacterAnimation _characterAnimation;
    private Movement _movement;
    private Combat _combat;
    private float _horizontalMove;
    private bool _isGrounded;

    private void Awake()
    {
        _levelGoalController = FindObjectOfType<LevelGoalController>();
        _characterAnimation = GetComponentInChildren<CharacterAnimation>();
        _movement = GetComponent<Movement>();
        _combat = GetComponent<Combat>();
    }

    private void Update()
    {
        _horizontalMove = Input.GetAxisRaw("Horizontal");
        if (Input.GetButton("Horizontal"))
        {
            _movement.Run(speed, _horizontalMove);
        }
        if (_isGrounded && Input.GetButtonDown("Jump"))
        {
            _movement.Jump(jumpForce);
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            _combat.MeleeAttack(attackDamage, attackRange, attackDelay);
        }
    }

    private void FixedUpdate()
    {
        _characterAnimation.Speed = Math.Abs(_horizontalMove);
        _isGrounded = footCollider.IsTouchingLayers(ground) ? true : false;
        _characterAnimation.IsJumping = !_isGrounded;
        if (!_isGrounded && transform.position.y < deathHeightLevel)
        {
            _levelGoalController.BadEndTheGame();
        }
    }
}
