using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelGoalController : MonoBehaviour
{
    [SerializeField] private GameObject endGameScreen;
    [SerializeField] private Button restartButton;
    [SerializeField] private Button exitGameButton;
    [SerializeField] private TextMeshProUGUI timeText;
    private float _levelTime;
    private int _spawnedEnemies;
    private int _killedEnemies;

    private void Awake()
    {
        restartButton.onClick.AddListener(RestartGame);
        exitGameButton.onClick.AddListener(ExitGame);
        _levelTime = Time.time;
    }


    private void GoodEndTheGame()
    {
        endGameScreen.SetActive(true);
        timeText.text += string.Format("{0:f2}", Time.time - _levelTime);
    }

    public void BadEndTheGame()
    {
        endGameScreen.SetActive(true);
        timeText.text = "You died";
        timeText.color = Color.red;
    }

    public void SetSpawnedEnemies(int number)
    {
        _spawnedEnemies = number;
    }

    public void IncreaseKilledEnemies()
    {
        _killedEnemies++;
        if (_spawnedEnemies == _killedEnemies)
        {
            GoodEndTheGame();
        }
    }

    private void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void ExitGame()
    {
        Application.Quit();
    }
}