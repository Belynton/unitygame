using UnityEngine;

public class CharacterAnimation : MonoBehaviour
{
    private Animator _animator;

    public float Speed { private get; set; }
    public bool IsJumping { private get; set; }

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    private void Update() 
    {
        _animator.SetFloat("Speed", Speed);
        _animator.SetBool("IsJumping", IsJumping);
    }

    public void MeleeAttack()
    {
        _animator.SetTrigger("MeleeAttack");
    }
}
