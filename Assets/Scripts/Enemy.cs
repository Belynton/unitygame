using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private int maxHealth = 5;
    private int _currentHealth;
    private LevelGoalController _levelGoalController;

    private void Awake()
    {
        _currentHealth = maxHealth;
        _levelGoalController = FindObjectOfType<LevelGoalController>();
    }

    public void GetDamage(int damage)
    {
        _currentHealth -= damage;
        if (_currentHealth <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        _levelGoalController.IncreaseKilledEnemies();
        Destroy(this.gameObject);
    }

    public int GetHealth() 
    {
        return _currentHealth;
    }
}
